<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class ListUserController extends Controller {
    public function listUser() {
        $list = User::all();
        return response()->json($list);
    }
}
